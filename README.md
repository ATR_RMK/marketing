# [GitLab Marketing](https://gitlab-com.gitlab.io/marketing)

All things marketing.

- [Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/issues)
- [Marketing handbook](https://about.gitlab.com/handbook/marketing/)

## Repository SUPERPOWERS :stars:

*Please put all exported folders in the `hosted` directory. That way you can shuffle the rest of your repository around without breaking links!*

- __Semi automatic Sketch spec previews with Continuous Integration__

*Whenever you create a spec preview folder with the [Sketch Measure Plugin](https://github.com/utom/sketch-measure), append `spec-previews` to the name of the generated directory and it will be visible by an URL. Search in [https://gitlab-com.gitlab.io/marketing/](https://gitlab-com.gitlab.io/marketing/)*

- __Automatic live Framer prototypes with Continuous Integration__

*Whenever you save a [Framer](https://framerjs.com) prototype in this repository and commit push it to GitLab, it will automatically be hosted in the same way as the spec previews superpower. See them live at [https://gitlab-com.gitlab.io/marketing/](https://gitlab-com.gitlab.io/marketing/)*

- __Standalone Live Html Previews__

*By using the wget command: `wget -kN --html-extension URL` or `wget -E -p -k URL` you can create a standalone working HTML page of the GitLab view you want. Just change the name of the file to index.html and append `html-previews` to the name of the directory it will be inside of. Good luck! (No guarantees with this one!)
