### Overview

Our partner activation framework consists of the following action items within the below issue. For each action item within this issue, a separate issue should be created by the assignee who is responsible for that action item within the allocated timeframe.

### Goal

Drive awareness of new strategic partnerships through marketing activities, and increase user adoption of technical integrations. 


### Week 1: Announcing Partnership

- [ ] *Product Marketing* @scornille & @eliran.mesika - After a strategic partnership is signed, a press release should be created announcing the partnership, pulling out the following messages:
-       Value of partnership - what it enables for our users and why we chose to partner
-       How the partnership allows our users to "achieve software excellence" - messaging framework
-       Quote from strategic partner & Director of Strategic Partnerships
-       Link to partner's website
- [ ] *Product Marketing* @scornille Publish on [press release page](https://about.gitlab.com/press/releases/), request that partner publish press release too.
- [ ] *Product Marketing* @scornille - Request that GitLab is added to partner's website if they have a partner page
- [ ] *Partner* - Add logo and company information to GitLab partner [application page](https://about.gitlab.com/applications/) by following instructions on this [page](https://about.gitlab.com/partners/)

### Week 2 & 3: Generating Awareness

- [ ] *Product Marketing* @scornille - Provide Content Marketing with details about partnership - Why partnership is meaningful, impact on industry, alignment with GitLab messaging, why should people care.
- [ ] *Product Marketing* @scornille - Introduce Content Marketing team to Partner
- [ ] *Content Marketing* @erica - Blog post building on the press release with a focus on thought leadership. Publish on [website](https://about.gitlab.com/blog/), include in monthly company newsletter, request that partner publish blog post in their own newsletter
- [ ] *Content Marketing* @scornille Collect and store blog post metrics to assess awareness of partnership
- [ ] *Product Marketing* @scornille - Share clickthrough metrics to partner website from newsletter & blog post page with partner 7 days following newsletter being sent out

### Month 2 & 3 Generating Awareness Continued

- [ ] *Product Marketing* @scornille - Ask partner if interested in participating in joint webinar. If partner is interested, pass to Content Marketing to arrange.
- [ ] *Content Marketing* @erica - Dependant on previous action item: Webinar should be focused on thought leadership, industry direction, with a balance of a demo of the partner integration 
- [ ] *Content Marketing* @erica - Collect and store webinar leads driven by webinar in Marketo. Share with Product Marketing if/when lead converts.
- [ ] *Product Marketing* @scornille - Request customer case study if partner has an eligible customer, and begin [customer case study creation process](https://about.gitlab.com/handbook/marketing/product-marketing/#case-study)

### Month 4-6 Generating Awareness Continued

- [ ] *Product Marketing* @scornille - Assess whether second webinar with partner & customer is feasible based on feedback & success of first webinar
- [ ] *Content Marketing* @erica - Dependant on above action item: Depending on success of first webinar: Host webinar with partner and one of their customers that uses both of our products and has adopted the integration. Request customer to present

### Ongoing Efforts

- Attend events together, joint workshop sessions, speak at eachother's meet-ups
