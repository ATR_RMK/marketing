 ### Release Checklist
 
 - [ ] Verify that the work for the last release was documented and issue was closed
 - [ ] Release issue using the release label was created in the marketing project
 - [ ] Priority features that were agreed upon in the release kickoff were mentioned in this release issue
 - [ ] Priority features have been scheduled or demoed in GitLab University
 - [ ] Marketing plan for priority features has been documented
 - [ ] Priority features are fully documented with screenshots (align with technical writers)
 - [ ] Priority features are shown on /features or updated in other website pages (no changes to site in this release)
 - [ ] Priority features appear in the compare view on /features
 - [ ] Evaluate the opportunity to update existing screens or copy on pages that will be updated for the release
 - [ ] Document changes made to the site in the release issue
 
  ### Priority Features
  
  Outline the priority features and share insight into marketing plan
  