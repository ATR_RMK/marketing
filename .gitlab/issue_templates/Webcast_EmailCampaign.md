For #_link to main issue_.

* [ ]  Discuss segmentation for invitation emails
* [ ]  @rebecca to provide email copy _link to Google Doc with email copy_
* [ ]  Send invitation email 1 (_date for emails to be sent_)
* [ ]  Send invitation email 2 (_date for emails to be sent_)
* [ ]  Send invitation email 3 (_date for emails to be sent_)
* [ ]  Send reminder email 1 (_date for emails to be sent_)
* [ ]  Send reminder email 2 (_date for emails to be sent_)
* [ ]  Send reminder email 3 (_date for emails to be sent_)
* [ ]  Send recording emails (_date for emails to be sent_)